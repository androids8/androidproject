# Studio Ghibli Android Project
<!-- Introduction du projet -->
This project have been designed and developped by [`Tristan CLEMENCEAU`](https://gitlab.com/tristann) and [`Alexandre GOMEZ`](https://gitlab.com/Alexgoar). 

## Summary
<!-- liens relatifs Example -->
- [About](#about)
- [Prerequisite](#Prerequisite)
- [Contributing](#contributing)
- [Contacts](#contacts)
- [Links](#links)
- [License](#LICENSE)

## About

The application gives you all the informations you need to know everything about the movies of Studio Ghibli like the peoples, the locations, the species and the vehicles. Studio Ghibli is a well known japanese animation film studio.

For the data, the application uses the API of [Studio Ghibli](https://ghibliapi.herokuapp.com/) for all the informations about the movies, peoples, locations, species and vehicles that can be found in the Ghibli universe. It also uses the API of [Wallhaven](https://wallhaven.cc/help/api) to get all the images because the API of Studio Ghibli does not offer any images.

This application has been built with JAVA on Android Studio

## Prerequisite

Make sure you have installed all of the following prerequisites on your development machine:

* [Android Studio](https://developer.android.com/studio)
* Optionnal : [Git](https://git-scm.com/downloads) (you can also download the project directly instead of cloning it)

## How to launch the app

To launch the app, add the project to Android Studio (by cloning it with git or download). 
Clean and build the application first, then run it on your emulator. We recommend to run it on API 26 versions. If it is your first time running an application on Android Studio, run the AVD Manager and create a new virtual device, the application works on every phones available, and select Oreo(Android 8.0).

## Contacts

Something

## Links

Something

# LICENSE

This project is licensed under the MIT License - see the LICENSE.md file for details
