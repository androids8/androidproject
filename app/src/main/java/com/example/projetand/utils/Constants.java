package com.example.projetand.utils;

/**
 * This class is used to stored all the constant of the program.
 * If you modify one value here all the program will change.
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class Constants {
    /*GENERAL*/
    private static final String PROPERTIES_URL_API_BASE = "https://ghibliapi.herokuapp.com";
    public static final String PROPERTIES_API_KEY = "Ukel8WoVvipVWJ3JB8jpjv3BsHkriCeP";
    public static final String PROPERTIES_URL_API_SEARCH = "https://wallhaven.cc/api/v1/search?q=";
    public static final String PROPERTIES_URL_API_FILMS = String.format("%s/%s",PROPERTIES_URL_API_BASE,"films");
    public static final String PROPERTIES_URL_API_PEOPLE = String.format("%s/%s",PROPERTIES_URL_API_BASE,"people");
    public static final String PROPERTIES_URL_API_LOCATIONS = String.format("%s/%s",PROPERTIES_URL_API_BASE,"locations");
    public static final String PROPERTIES_URL_API_SPECIES = String.format("%s/%s",PROPERTIES_URL_API_BASE,"species");
    public static final String PROPERTIES_URL_API_VEHICLES = String.format("%s/%s",PROPERTIES_URL_API_BASE,"vehicles");
    public static final String PROPERTIES_DEFAULT_TRUE = "true";
    public static final String PROPERTIES_DEFAULT_FALSE = "false";
    public static final String PROPERTIES_DEFAULT_IMG_URL = "https://w.wallhaven.cc/full/4v/wallhaven-4vpp2p.jpg";
    public static final String[] PROPERTIES_DEFAULTS_ENDPOINTS = {"films","people","locations","species","vehicles"};
    public static final String PROPERTIES_TOOLBAR_SETTING = "Settings";
    public static final String PROPERTIES_TOOLBAR_HOME = "Home";
    public static final String PROPERTIES_TOOLBAR_MOVIE = "Movie";
    public static final String PROPERTIES_TOOLBAR_PEOPLE = "People";
    public static final String PROPERTIES_TOOLBAR_SPECIES = "Species";
    public static final String PROPERTIES_TODO = "todo";

    /*TAG*/
        //CLASSNAME
        public static final String TAG_CLASSNAME_HELPER = "HELPER";
        public static final String TAG_CLASSNAME_ASYNCGHIBLIJSON = "ASYNCGHIBLIJSON";
        public static final String TAG_CLASSNAME_MAINACTIVITY = "MAINACTIVITY";
        public static final String TAG_CLASSNAME_ASYNCGHIBLIJSONURL = "ASYNCGHIBLIJSONURL";
        public static final String TAG_CLASSNAME_SETTINGS = "SETTINGS";
        public static final String TAG_CLASSNAME_HOMEACTIVITY = "HOME";
        public static final String TAG_CLASSNAME_DETAILSACTIVITY = "DETAILSACTIVITY";
        public static final String TAG_CLASSNAME_ASYNCBITMAPDOWNLOADER = "ASYNCBITMAPDOWNLOADER";
        public static final String TAG_CLASSNAME_CUSTOMSERVICE = "CUSTOMSERVICE";
        public static final String TAG_CLASSNAME_DATABASE = "DATABASE";

        //METHODS & FUNCTIONS
        public static final String TAG_METFUNC_READSTREAM= "READSTREAM";
        public static final String TAG_METFUNC_ONCLICK= "ONCLICK";
        public static final String TAG_METFUNC_DOINBACKGROUND= "DOINBACKGROUND";
        public static final String TAG_METFUNC_ONPOSTEXECUTE= "ONPOSTEXECUTE";
        public static final String TAG_METFUNC_ONOPTIONSITEMSELECTED= "ONOPTIONSITEMSELECTED";
        public static final String TAG_METFUNC_ONNAVIGATIONITEMSELECTED = "ONNAVIGATIONITEMSELECTED";
        public static final String TAG_METFUNC_LOADDATA = "LOADDATA";
        public static final String TAG_METFUNC_ONFINISH = "ONFINISH";
        public static final String TAG_METFUNC_ONDESTROY = "ONDESTROY";
        public static final String TAG_METFUNC_ONREBIND = "ONREBIND";
        public static final String TAG_METFUNC_ONSTARTCOMMAND = "ONSTARTCOMMAND";
        public static final String TAG_METFUNC_PERFORMTASKINFACTOFINTENT = "PERFORMTASKINFACTOFINTENT";
        public static final String TAG_METFUNC_LOADDEFAULTJSON = "LOADDEFAULTJSON";
        public static final String TAG_METFUNC_FILEREAD = "FILEREAD";
        public static final String TAG_METFUNC_FILESAVE= "FILESAVE";
        public static final String TAG_METFUNC_GETJSONOBJECTFROMID = "GETJSONOBJECTFROMID";
        public static final String TAG_METFUNC_LOADITEMS = "LOADITEMS";
        public static final String TAG_METFUNC_GETMOVIES = "GETMOVIES";
        public static final String TAG_METFUNC_GETPEOPLE = "GETPEOPLE";
        public static final String TAG_METFUNC_GETSPECIES = "GETSPECIES";


    /*TOKEN*/
        //JSON
        public static final String JSONTOKEN_DATA = "data";
        public static final String JSONTOKEN_PATH = "path";
        public static final String JSONTOKEN_TYPE_OBJECT= "type_object";
        public static final String JSONTOKEN_FILMS = "films";
        public static final String JSONTOKEN_PEOPLE = "people";
        public static final String JSONTOKEN_LOCATIONS = "locations";
        public static final String JSONTOKEN_SPECIES = "species";
        public static final String JSONTOKEN_VEHICLES = "vehicles";
        public static final String JSONTOKEN_NAME = "name";
        public static final String JSONTOKEN_ID = "id";
        public static final String JSONTOKEN_ORIGINAL_TITLE_ROMAN = "original_title_romanised";
        public static final String JSONTOKEN_TITLE = "title";
        public static final String JSONTOKEN_ORIGINAL_TITLE = "original_title";
        public static final String JSONTOKEN_DESCRIPTION = "description";
        public static final String JSONTOKEN_PRODUCER = "producer";
        public static final String JSONTOKEN_DIRECTOR = "director";
        public static final String JSONTOKEN_RELEASE = "release_date";
        public static final String JSONTOKEN_RUNNING_TIME = "running_time";
        public static final String JSONTOKEN_SCORE = "rt_score";
        public static final String JSONTOKEN_EYE_COLOR = "eye_color";
        public static final String JSONTOKEN_HAIR_COLOR = "hair_color";
        public static final String JSONTOKEN_CLASSIFICATION = "classification";
        public static final String JSONTOKEN_EYE_COLORS = "eye_colors";
        public static final String JSONTOKEN_HAIR_COLORS = "hair_colors";
        public static final String JSONTOKEN_GENDER = "gender";
        public static final String JSONTOKEN_AGE = "age";
        public static final String JSONTOKEN_CLIMATE = "climate";
        public static final String JSONTOKEN_TERRAIN = "terrain";
        public static final String JSONTOKEN_SURFACE_WATER = "surface_water";
        public static final String JSONTOKEN_RESIDENTS = "residents";
        public static final String JSONTOKEN_VEHICLE_CLASS = "vehicle_class";
        public static final String JSONTOKEN_LENGTH = "length";
        public static final String JSONTOKEN_PILOT = "pilot";
        public static final String JSONTOKEN_KEYWORDS = "keywords";
        public static final String JSONTOKEN_OBJECTS = "objects";

        //EXTRA
        public static final String EXTRATOKEN_ID_ITEM = "id_item";
        public static final String EXTRATOKEN_SERVICE_VALUE = "data_service_value";
        public static final String EXTRATOKEN_SERVICE_JSON = "data_service_json";

        //SHAREDPREFERENCES
        public static final String SHAREDPREFTOKEN_SHAREDPREF = "SharedPref";
        public static final String SHAREDPREFTOKEN_DARKMODE = "isDarkMode";


    /*MSG*/
        //INFO
        public static final String MSG_INFO_DEFAULTCASE = "Default case, to prevent unwanted cases";
        public static final String MSG_INFO_ACTIVITY_KILLED = "Activity killed";
        public static final String MSG_INFO_SERVICE_STOPPED = "Service killed";
        public static final String MSG_INFO_SERVICE_PERSMISSION = "No permissions granted";

        //ERROR
        public static final String MSG_ERROR_REASON = "Reason";

}
