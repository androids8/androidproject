package com.example.projetand.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import static com.example.projetand.helpers.Helpers.*;
import static com.example.projetand.utils.Constants.*;

/**
 * This class extends from the Service class. It let us start and manage our custom service
 * where we get the history of searches of the user
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class CustomService extends Service {

    private String filename = "history.json";
    private JSONObject root = new JSONObject();


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        loadDefaultJSON();
        Context context = getApplicationContext();

        if(!checkPermissionGranted(context)){
            File file = new File(getExternalFilesDir("MyFileStorage"),filename);

            if(file.exists()){
                //Read data
                fileRead(file);
            }
            //Implement data given
            performTaskInFactOfIntent(intent);
            //Save data
            fileSave(file);

        }else{
            Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_CUSTOMSERVICE,TAG_METFUNC_ONSTARTCOMMAND),MSG_INFO_SERVICE_PERSMISSION);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_CUSTOMSERVICE,TAG_METFUNC_ONDESTROY),MSG_INFO_SERVICE_STOPPED);
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    public boolean onUnbind(Intent intent) {

        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        loadDefaultJSON();
        Context context = getApplicationContext();

        if(!checkPermissionGranted(context)){
            File file = new File(getExternalFilesDir("MyFileStorage"),filename);

            if(file.exists()){
                //Read data
                fileRead(file);
            }
            //Implement data given
            performTaskInFactOfIntent(intent);
            //Save data
            fileSave(file);

        }else{
            Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_CUSTOMSERVICE,TAG_METFUNC_ONREBIND),MSG_INFO_SERVICE_PERSMISSION);
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    private void performTaskInFactOfIntent(Intent intent) {
        //Change state according to the intent value

        String serviceValue = intent.getStringExtra(EXTRATOKEN_SERVICE_VALUE);
        String serviceJson = intent.getStringExtra(EXTRATOKEN_SERVICE_JSON);
        boolean performTask = serviceValue.isEmpty();

        if(!performTask){
            //No empty data
            try {
                if(serviceJson.toLowerCase().equals(PROPERTIES_DEFAULT_FALSE)){
                    //Keyword
                    root.getJSONArray(JSONTOKEN_KEYWORDS).put(serviceValue);
                }else{
                    //Id object
                    root.getJSONArray(JSONTOKEN_OBJECTS).put(serviceValue);
                }
            } catch (JSONException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_CUSTOMSERVICE,TAG_METFUNC_PERFORMTASKINFACTOFINTENT), e.getMessage(), e);
            }
        }
    }

    /**
     * This method load a default JSON Object to avoid a bug
     */
    private void loadDefaultJSON(){

        try {
            if(!(root.has(JSONTOKEN_KEYWORDS) && root.has(JSONTOKEN_OBJECTS))){
                //Creating object
                JSONArray keywords = new JSONArray();
                JSONArray objects = new JSONArray();

                root.put(JSONTOKEN_KEYWORDS,keywords);
                root.put(JSONTOKEN_OBJECTS,objects);
            }
        } catch (JSONException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_CUSTOMSERVICE,TAG_METFUNC_LOADDEFAULTJSON), e.getMessage(), e);
        }
    }

    /**
     * This method reads the file put in parameter and it populates the JSON object int the attributes
     *
     * @param file that we want to read
     */

    private void fileRead(File file){
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            StringBuilder stringBuilder = new StringBuilder();
            String line = bufferedReader.readLine();
            while (line != null){
                stringBuilder.append(line).append("\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();

            String response = stringBuilder.toString();
            root = new JSONObject(response);

        } catch (FileNotFoundException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_CUSTOMSERVICE,TAG_METFUNC_FILEREAD), e.getMessage(), e);
        } catch (IOException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_CUSTOMSERVICE,TAG_METFUNC_FILEREAD), e.getMessage(), e);
        } catch (JSONException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_CUSTOMSERVICE,TAG_METFUNC_FILEREAD), e.getMessage(), e);
        }
    }

    /**
     * This method save the JSON Object in the file given in parameter
     *
     * @param file that represent the target file to save our data
     */
    private void fileSave(File file){
        try {
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(root.toString(1));
            bufferedWriter.close();

        } catch (IOException | JSONException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_CUSTOMSERVICE,TAG_METFUNC_FILESAVE), e.getMessage(), e);
        }
    }
}
