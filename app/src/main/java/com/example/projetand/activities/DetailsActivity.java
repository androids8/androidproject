package com.example.projetand.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.example.projetand.R;
import com.example.projetand.asynctask.AsyncGhibliJSONUrl;
import com.example.projetand.models.Item;

import static com.example.projetand.helpers.Helpers.*;
import static com.example.projetand.models.Database.*;
import static com.example.projetand.utils.Constants.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class is an activity used to display every details of the item wanted by the user
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */


public class DetailsActivity extends AppCompatActivity {

    private TextView lbl_title;
    private ImageView image;
    private TextView lbl_content;

    private Toolbar tlb_details;
    private JSONObject item;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        lbl_title = (TextView)findViewById(R.id.lbl_title);
        lbl_content = (TextView)findViewById(R.id.lbl_content);
        tlb_details = findViewById(R.id.tlb_details);
        image = findViewById(R.id.imageMovie);

        Intent intent = getIntent();
        item = getJSONObjectFromId(intent.getStringExtra(EXTRATOKEN_ID_ITEM));

        loadData();

        setSupportActionBar(tlb_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);

                finish();
                return true;
            default:
                Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_DETAILSACTIVITY,TAG_METFUNC_ONNAVIGATIONITEMSELECTED),MSG_INFO_DEFAULTCASE);
                return true;
        }
    }

    /**
     * This Method check the type of the item and load the data depending on the item's type
     */
    private void loadData(){
        try {
            String type = item.getString(JSONTOKEN_TYPE_OBJECT);

            switch (type.toLowerCase()){
                case JSONTOKEN_FILMS:
                    formatedDisplayFilms();
                    break;
                case JSONTOKEN_PEOPLE:
                    formatedDisplayPeople();
                    break;
                case JSONTOKEN_LOCATIONS:
                    formatedDisplayLocations();
                    break;
                case JSONTOKEN_VEHICLES:
                    formatedDisplayVehicles();
                    break;
                case JSONTOKEN_SPECIES:
                    formatedDisplaySpecies();
                    break;
                default:
                    Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_DETAILSACTIVITY,TAG_METFUNC_LOADDATA),MSG_INFO_DEFAULTCASE);
            }

        } catch (JSONException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_DETAILSACTIVITY,TAG_METFUNC_LOADDATA), e.getMessage(), e);
        }
    }

    /**
     * This method format how the data is displayed in the TextView if the item is a film
     * @throws JSONException
     */
    private void formatedDisplayFilms() throws JSONException {
        //Var
        String title = item.getString(JSONTOKEN_ORIGINAL_TITLE_ROMAN);
        StringBuilder sb = new StringBuilder();
        JSONArray arrayObj;
        int cpt = 0;

        //Toolbar & Title
        tlb_details.setTitle(title);
        lbl_title.setText(Html.fromHtml(String.format("<b>%s</b>",title),Html.FROM_HTML_MODE_LEGACY));

        //Image
        new AsyncGhibliJSONUrl(getItemFromId(item.getString(JSONTOKEN_ID)),image).execute(formatUrlFromName(item.getString(JSONTOKEN_TITLE)));

        //Content
        sb.append(String.format("<h3>%s :</h3>","Movie's information"));
        sb.append(String.format("<b>%s :</b> %s <b>%s :</b> %s<br />","Title",item.getString(JSONTOKEN_TITLE),"Original title",item.getString(JSONTOKEN_ORIGINAL_TITLE)));
        sb.append("<br />");
        sb.append(String.format("<b>%s :</b> %s<br />","Description",item.getString(JSONTOKEN_DESCRIPTION)));
        sb.append("<br />");
        sb.append(String.format("<b>%s :</b> %s <b>%s :</b> %s<br />","Director",item.getString(JSONTOKEN_DIRECTOR),"Producer",item.getString(JSONTOKEN_PRODUCER)));
        sb.append(String.format("<b>%s :</b> %s <b>%s :</b> %s <b>%s :</b> %s/100<br />","Release date",item.getString(JSONTOKEN_RELEASE),"Running time",item.getString(JSONTOKEN_RUNNING_TIME),"Score",item.getString(JSONTOKEN_SCORE)));
        sb.append("<br />");
        sb.append(String.format("<h3>%s :</h3>","Other information"));

        //People
        sb.append(String.format("<b>%s :</b><br />","People"));
        arrayObj = item.getJSONArray(JSONTOKEN_PEOPLE);

        for(int i = 0 ; i< arrayObj.length();i++){
            String url = arrayObj.getString(i);
            if(isUrlValid(url)){
                Item item = getItemFromId(getIdFromUrl(url));
                cpt++;
                sb.append(String.format("%s, ",item.getName()));
            }
        }

        if(cpt == 0){
            sb.append(String.format("%s<br />","No people"));
        }else{
            sb.append("<br />");
        }

        cpt = 0;

        //Species
        sb.append(String.format("<b>%s :</b><br />","Species"));
        arrayObj = item.getJSONArray(JSONTOKEN_SPECIES);

        for(int i = 0 ; i< arrayObj.length();i++){
            String url = arrayObj.getString(i);
            if(isUrlValid(url)){
                Item item = getItemFromId(getIdFromUrl(url));
                cpt++;
                sb.append(String.format("%s, ",item.getName()));
            }
        }

        if(cpt == 0){
            sb.append(String.format("%s<br />","No species"));
        }else{
            sb.append("<br />");
        }

        cpt = 0;

        //Location
        sb.append(String.format("<b>%s :</b><br />","Locations"));
        arrayObj = item.getJSONArray(JSONTOKEN_LOCATIONS);

        for(int i = 0 ; i< arrayObj.length();i++){
            String url = arrayObj.getString(i);
            if(isUrlValid(url)){
                Item item = getItemFromId(getIdFromUrl(url));
                cpt++;
                sb.append(String.format("%s, ",item.getName()));
            }
        }

        if(cpt == 0){
            sb.append(String.format("%s<br />","No locations"));
        }else{
            sb.append("<br />");
        }

        cpt = 0;

        //Vehicles
        sb.append(String.format("<b>%s :</b><br />","Vehicles"));
        arrayObj = item.getJSONArray(JSONTOKEN_VEHICLES);

        for(int i = 0 ; i< arrayObj.length();i++){
            String url = arrayObj.getString(i);
            if(isUrlValid(url)){
                Item item = getItemFromId(getIdFromUrl(url));
                cpt++;
                sb.append(String.format("%s, ",item.getName()));
            }
        }

        if(cpt == 0){
            sb.append(String.format("%s<br />","No vehicles"));
        }else{
            sb.append("<br />");
        }

        sb.append("<br />");
        lbl_content.setText(Html.fromHtml(sb.toString(),Html.FROM_HTML_MODE_LEGACY));

    }

    /**
     * This method format how the data is displayed in the TextView if the item is a location
     * @throws JSONException
     */

    private void formatedDisplayLocations() throws JSONException {
        //Var
        String title = item.getString(JSONTOKEN_NAME);
        StringBuilder sb = new StringBuilder();
        JSONArray arrayObj;
        int cpt = 0;

        //Toolbar & Title
        tlb_details.setTitle(title);
        lbl_title.setText(Html.fromHtml(String.format("<b>%s</b>",title),Html.FROM_HTML_MODE_LEGACY));

        //Image
        new AsyncGhibliJSONUrl(getItemFromId(item.getString(JSONTOKEN_ID)),image).execute(formatUrlFromName(title));

        //Content
        sb.append(String.format("<h3>%s :</h3>","Location's information"));
        sb.append(String.format("<b>%s :</b> %s <b>%s :</b> %s<br />","Climate",item.getString(JSONTOKEN_CLIMATE),"Terrain",item.getString(JSONTOKEN_TERRAIN)));
        sb.append(String.format("<b>%s :</b> %s<br />","Surface water",item.getString(JSONTOKEN_SURFACE_WATER)));
        sb.append("<br />");
        sb.append(String.format("<h3>%s :</h3>","Other information"));

        //Residents
        sb.append(String.format("<b>%s :</b><br />","Residents"));
        arrayObj = item.getJSONArray(JSONTOKEN_RESIDENTS);

        for(int i = 0 ; i< arrayObj.length();i++){
            String url = arrayObj.getString(i);
            if(isUrlValid(url) && !url.toLowerCase().equals(PROPERTIES_TODO)){
                Item item = getItemFromId(getIdFromUrl(url));
                cpt++;
                sb.append(String.format("%s, ",item.getName()));
            }
        }

        if(cpt == 0){
            sb.append(String.format("%s<br />","No residents"));
        }else{
            sb.append("<br />");
        }

        cpt = 0;

        //Films
        sb.append(String.format("<b>%s :</b><br />","Films"));
        arrayObj = item.getJSONArray(JSONTOKEN_FILMS);

        for(int i = 0 ; i< arrayObj.length();i++){
            String url = arrayObj.getString(i);
            if(isUrlValid(url)){
                Item item = getItemFromId(getIdFromUrl(url));
                cpt++;
                sb.append(String.format("%s, ",item.getName()));
            }
        }

        if(cpt == 0){
            sb.append(String.format("%s<br />","No films"));
        }else{
            sb.append("<br />");
        }


        sb.append("<br />");
        lbl_content.setText(Html.fromHtml(sb.toString(),Html.FROM_HTML_MODE_LEGACY));

    }

    /**
     * This method format how the data is displayed in the TextView if the item is a vehicle
     * @throws JSONException
     */

    private void formatedDisplayVehicles()  throws JSONException{
        //Var
        String title = item.getString(JSONTOKEN_NAME);
        StringBuilder sb = new StringBuilder();
        JSONArray arrayObj;
        int cpt = 0;

        //Toolbar & Title
        tlb_details.setTitle(title);
        lbl_title.setText(Html.fromHtml(String.format("<b>%s</b>",title),Html.FROM_HTML_MODE_LEGACY));

        //Image
        new AsyncGhibliJSONUrl(getItemFromId(item.getString(JSONTOKEN_ID)),image).execute(formatUrlFromName(title));

        //Content
        sb.append(String.format("<h3>%s :</h3>","Vehicle's information"));
        sb.append(String.format("<b>%s :</b> %s<br />","Description",item.getString(JSONTOKEN_DESCRIPTION)));
        sb.append(String.format("<b>%s :</b> %s <b>%s :</b> %s<br />","Vehicle class",item.getString(JSONTOKEN_VEHICLE_CLASS),"Length",item.getString(JSONTOKEN_LENGTH)));
        sb.append("<br />");
        sb.append(String.format("<h3>%s :</h3>","Other information"));

        //Pilot
        sb.append(String.format("<b>%s :</b>","Pilot"));
        String pilot = item.getString(JSONTOKEN_PILOT);

        if(isUrlValid(pilot) && !pilot.toLowerCase().equals(PROPERTIES_TODO)){
            Item item = getItemFromId(getIdFromUrl(pilot));
            sb.append(String.format(" %s<br /> ",item.getName()));
        }else{
            sb.append(String.format("%s<br />","No pilot"));
        }

        //Films
        sb.append(String.format("<b>%s :</b><br />","Films"));
        arrayObj = item.getJSONArray(JSONTOKEN_FILMS);

        for(int i = 0 ; i< arrayObj.length();i++){
            String url = arrayObj.getString(i);
            if(isUrlValid(url)){
                Item item = getItemFromId(getIdFromUrl(url));
                cpt++;
                sb.append(String.format("%s, ",item.getName()));
            }
        }

        if(cpt == 0){
            sb.append(String.format("%s<br />","No films"));
        }else{
            sb.append("<br />");
        }


        sb.append("<br />");
        lbl_content.setText(Html.fromHtml(sb.toString(),Html.FROM_HTML_MODE_LEGACY));
    }

    /**
     * This method format how the data is displayed in the TextView if the item is a specie
     * @throws JSONException
     */
    private void formatedDisplaySpecies()  throws JSONException{
        //Var
        String title = item.getString(JSONTOKEN_NAME);
        StringBuilder sb = new StringBuilder();
        JSONArray arrayObj;
        int cpt = 0;

        //Toolbar & Title
        tlb_details.setTitle(title);
        lbl_title.setText(Html.fromHtml(String.format("<b>%s</b>",title),Html.FROM_HTML_MODE_LEGACY));

        //Image
        new AsyncGhibliJSONUrl(getItemFromId(item.getString(JSONTOKEN_ID)),image).execute(formatUrlFromName(title));

        //Content
        sb.append(String.format("<h3>%s :</h3>","Specie's information"));
        sb.append(String.format("<b>%s :</b> %s<br />","Classification",item.getString(JSONTOKEN_CLASSIFICATION)));
        sb.append(String.format("<b>%s :</b> %s<br />","Eye colors",item.getString(JSONTOKEN_EYE_COLORS)));
        sb.append(String.format("<b>%s :</b> %s<br />","Hair colors",item.getString(JSONTOKEN_HAIR_COLORS)));
        sb.append("<br />");
        sb.append(String.format("<h3>%s :</h3>","Other information"));

        //People
        sb.append(String.format("<b>%s :</b><br />","People"));
        arrayObj = item.getJSONArray(JSONTOKEN_PEOPLE);

        for(int i = 0 ; i< arrayObj.length();i++){
            String url = arrayObj.getString(i);
            if(isUrlValid(url)){
                Item item = getItemFromId(getIdFromUrl(url));
                cpt++;
                sb.append(String.format("%s, ",item.getName()));
            }
        }

        if(cpt == 0){
            sb.append(String.format("%s<br />","No people"));
        }else{
            sb.append("<br />");
        }

        cpt = 0;


        //Films
        sb.append(String.format("<b>%s :</b><br />","Films"));
        arrayObj = item.getJSONArray(JSONTOKEN_FILMS);

        for(int i = 0 ; i< arrayObj.length();i++){
            String url = arrayObj.getString(i);
            if(isUrlValid(url)){
                Item item = getItemFromId(getIdFromUrl(url));
                cpt++;
                sb.append(String.format("%s, ",item.getName()));
            }
        }

        if(cpt == 0){
            sb.append(String.format("%s<br />","No films"));
        }else{
            sb.append("<br />");
        }

        sb.append("<br />");
        lbl_content.setText(Html.fromHtml(sb.toString(),Html.FROM_HTML_MODE_LEGACY));

    }

    /**
     * This method format how the data is displayed in the TextView if the item is a people
     * @throws JSONException
     */
    private void formatedDisplayPeople()  throws JSONException{
        //Var
        String title = item.getString(JSONTOKEN_NAME);
        StringBuilder sb = new StringBuilder();
        JSONArray arrayObj;
        int cpt = 0;

        //Toolbar & Title
        tlb_details.setTitle(title);
        lbl_title.setText(Html.fromHtml(String.format("<b>%s</b>",title),Html.FROM_HTML_MODE_LEGACY));

        //Image
        new AsyncGhibliJSONUrl(getItemFromId(item.getString(JSONTOKEN_ID)),image).execute(formatUrlFromName(title));

        //Content
        sb.append(String.format("<h3>%s :</h3>","Person's information"));
        sb.append(String.format("<b>%s :</b> %s <b>%s :</b> %s<br />","Gender",item.getString(JSONTOKEN_GENDER),"Age",item.getString(JSONTOKEN_AGE)));
        sb.append(String.format("<b>%s :</b> %s <b>%s :</b> %s<br />","Eye color",item.getString(JSONTOKEN_EYE_COLOR),"Hair color",item.getString(JSONTOKEN_HAIR_COLOR)));
        sb.append("<br />");
        sb.append(String.format("<h3>%s :</h3>","Other information"));

        //People
        sb.append(String.format("<b>%s :</b><br />","People"));

        //Films
        sb.append(String.format("<b>%s :</b><br />","Films"));
        arrayObj = item.getJSONArray(JSONTOKEN_FILMS);

        for(int i = 0 ; i< arrayObj.length();i++){
            String url = arrayObj.getString(i);
            if(isUrlValid(url)){
                Item item = getItemFromId(getIdFromUrl(url));
                cpt++;
                sb.append(String.format("%s, ",item.getName()));
            }
        }

        if(cpt == 0){
            sb.append(String.format("%s<br />","No films"));
        }else{
            sb.append("<br />");
        }

        //Pilot
        sb.append(String.format("<b>%s :</b>","Species"));
        String pilot = item.getString(JSONTOKEN_SPECIES);

        if(isUrlValid(pilot) && !pilot.toLowerCase().equals(PROPERTIES_TODO)){
            Item item = getItemFromId(getIdFromUrl(pilot));
            sb.append(String.format(" %s<br /> ",item.getName()));
        }else{
            sb.append(String.format("%s<br />","No species"));
        }

        sb.append("<br />");
        lbl_content.setText(Html.fromHtml(sb.toString(),Html.FROM_HTML_MODE_LEGACY));
    }

    /**
     * onStop method release the resources that are not needed anymore
     */
    @Override
    protected void onStop() {
        Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_DETAILSACTIVITY, TAG_METFUNC_ONFINISH), MSG_INFO_ACTIVITY_KILLED);
        super.onStop();
    }

}
