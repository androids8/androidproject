package com.example.projetand.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.MenuItem;

import com.example.projetand.R;
import com.example.projetand.fragments.HomeFragment;
import com.example.projetand.fragments.MovieFragment;
import com.example.projetand.fragments.PeopleFragment;
import com.example.projetand.fragments.SpeciesFragment;
import com.example.projetand.services.CustomService;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import static com.example.projetand.utils.Constants.*;

/**
 * This class is the home activity once the data is loaded we can display the items like the movies, peoples, locations, species and vehicles
 * Here we call the fragments Home, Movie, People or Specie depending of what the user want
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    private Toolbar tlb_main;
    private BottomNavigationView btm_menu;
    private FragmentManager frg_manager;
    private FragmentTransaction frg_transac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        tlb_main = (Toolbar) findViewById(R.id.tlb_main);
        btm_menu = (BottomNavigationView) findViewById(R.id.btm_menu);

        tlb_main.setTitle(PROPERTIES_TOOLBAR_HOME);

        setSupportActionBar(tlb_main);
        btm_menu.setOnNavigationItemSelectedListener(this);

        loadFragment(new HomeFragment());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itm_setting:
                Intent intent = new Intent(this,Settings.class);
                startActivity(intent);
                return true;
            default:
                Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_HOMEACTIVITY,TAG_METFUNC_ONOPTIONSITEMSELECTED),MSG_INFO_DEFAULTCASE);
                return true;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_home:
                tlb_main.setTitle(PROPERTIES_TOOLBAR_HOME);

                loadFragment(new HomeFragment());
                return true;
            case R.id.item_movie:
                tlb_main.setTitle(PROPERTIES_TOOLBAR_MOVIE);

                loadFragment(new MovieFragment());
                return true;
            case R.id.item_people:
                tlb_main.setTitle(PROPERTIES_TOOLBAR_PEOPLE);

                loadFragment(new PeopleFragment());
                return true;
            case R.id.item_species:
                tlb_main.setTitle(PROPERTIES_TOOLBAR_SPECIES);

                loadFragment(new SpeciesFragment());
                return true;
            default:
                Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_HOMEACTIVITY,TAG_METFUNC_ONNAVIGATIONITEMSELECTED),MSG_INFO_DEFAULTCASE);
                return true;
        }
    }

    /**
     * This method loads the fragment in the frame
     * @param fragment represent the fragment that will be displayed on the activity
     */

    private void loadFragment(Fragment fragment){
        frg_manager = getSupportFragmentManager();
        frg_transac = frg_manager.beginTransaction();
        frg_transac.replace(R.id.frm_frag,fragment);
        frg_transac.commit();
    }

    /**
     * onStop method release the resources that are not needed anymore
     */
    @Override
    protected void onStop() {
        Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_HOMEACTIVITY,TAG_METFUNC_ONFINISH),MSG_INFO_ACTIVITY_KILLED);

        Intent service = new Intent(this, CustomService.class);
        stopService(service);

        super.onStop();
    }
}