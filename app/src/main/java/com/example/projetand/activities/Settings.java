package com.example.projetand.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.CompoundButton;

import androidx.appcompat.widget.Toolbar;

import com.example.projetand.R;

import static com.example.projetand.helpers.Helpers.*;
import static com.example.projetand.utils.Constants.*;

/**
 * This class is the Activity settings, the user here can change whether he prefers Dark Mode or Light Mode
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class Settings extends AppCompatActivity {

    private SwitchCompat switch_drk_mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tlb_settings);
        switch_drk_mode = (SwitchCompat) findViewById(R.id.switch_drk_mode);

        changeModeOfApplication(this,switch_drk_mode);

        toolbar.setTitle(PROPERTIES_TOOLBAR_SETTING);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        switch_drk_mode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
                SaveData();
            }
        });
    }

    /**
     * This method save the state of the dark mode switch in the SharedPreferences file
     */
    public void SaveData(){
        SharedPreferences sharedPref = getSharedPreferences(SHAREDPREFTOKEN_SHAREDPREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(SHAREDPREFTOKEN_DARKMODE, switch_drk_mode.isChecked());
        editor.commit();
    }


    /**
     * Function used when an item is clicked on. Here the only button let the user come back to the HomeActivity
     * @param item the user clicked on
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_SETTINGS,TAG_METFUNC_ONOPTIONSITEMSELECTED),MSG_INFO_DEFAULTCASE);
                return true;
        }
    }

    /**
     * onStop method release the resources that are not needed anymore
     */
    @Override
    protected void onStop() {
        Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_SETTINGS,TAG_METFUNC_ONFINISH),MSG_INFO_ACTIVITY_KILLED);
        super.onStop();
    }
}