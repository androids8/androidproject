package com.example.projetand.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.projetand.R;
import com.example.projetand.asynctask.AsyncGhibliJSON;
import com.example.projetand.models.Database;

import static com.example.projetand.helpers.Helpers.askPermissions;
import static com.example.projetand.helpers.Helpers.checkPermissionGranted;
import static com.example.projetand.helpers.Helpers.*;
import static com.example.projetand.utils.Constants.*;

/**
 * This class is the first activity we go when we run the app.
 * Here we fetch and load the data from Ghibli
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressBar loadingBar;
    private TextView textViewResult;
    private Button btn_continue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        changeModeOfApplication(this,null);

        Database db = new Database();

        loadingBar = findViewById(R.id.LoadingBar);
        textViewResult = findViewById(R.id.textViewResult);
        btn_continue = findViewById(R.id.btn_continue);

        loadingBar.setMax(5);

        btn_continue.setOnClickListener(this);

        //Downloading data from Ghibli API
        new AsyncGhibliJSON(loadingBar,textViewResult,btn_continue).execute(PROPERTIES_URL_API_FILMS,PROPERTIES_URL_API_LOCATIONS,PROPERTIES_URL_API_PEOPLE,PROPERTIES_URL_API_SPECIES,PROPERTIES_URL_API_VEHICLES);

        //Asking permissions
        if(checkPermissionGranted(this)){
            askPermissions(this);
        }
    }

    /**
     * This method start the activity HomeActivity when the button is clicked on
     * @param v View that get clicked on, here the Continue button
     */

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_continue:
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);

                finish();
                break;
            default:
                Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_MAINACTIVITY,TAG_METFUNC_ONCLICK),MSG_INFO_DEFAULTCASE);
        }
    }

    /**
     * onStop method release the resources that are not needed anymore
     */
    @Override
    protected void onStop() {
        Log.i(String.format("[%s]-[%s] : ", TAG_CLASSNAME_MAINACTIVITY,TAG_METFUNC_ONFINISH),MSG_INFO_ACTIVITY_KILLED);
        super.onStop();
    }
}