package com.example.projetand.models;

import android.graphics.Bitmap;

/**
 * This class is where we create the object Item and implements Serializable
 * It is used to stock the id and the name of an item like a movie, a people, a location, a specie or a vehicle
 * We need it to pass the id and the name of the selected movie, people, etc in different Activity
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */
import java.io.Serializable;

public class Item implements Serializable {
    private Bitmap img;
    private String id,name;

    public Item(String id, String name) {
        this.img = null;
        this.id = id;
        this.name = name;
    }

    public Item(Bitmap img, String id, String name) {
        this.img = img;
        this.id = id;
        this.name = name;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
