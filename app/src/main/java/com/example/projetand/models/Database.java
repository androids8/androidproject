package com.example.projetand.models;

import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.projetand.utils.Constants.*;

/**
 * This class is where we load every single data of the Studio Ghibli's API
 * and manage them in our JSON Object and item list.
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class Database {

    private static JSONObject db;
    private static List<Item> items;
    private static List<Bitmap> bitmaps;

    public Database() {
        items = new ArrayList<Item>();
        bitmaps = new ArrayList<Bitmap>();
    }

    public static JSONObject getDb() {
        return db;
    }

    public static void setDb(JSONObject db) {
        Database.db = db;
    }

    public static List<Item> getItems() {
        return items;
    }

    //OTHER
        //SEARCH

        /**
         * This function get the JSONObject that have the id put in parameter
         *
         * @param id The id of the item we want to get the JSONObject from
         * @return the JSONObject with the id put in parameter and added a field type to let us know what is the type of the object
         */
        public static JSONObject getJSONObjectFromId(String id){
            JSONArray arrayObj;
            JSONObject res = null;
            boolean find = false;

            try {
                //films
                arrayObj = db.getJSONArray(JSONTOKEN_FILMS);

                for (int i = 0; i < arrayObj.length(); i++) {
                    if(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID).toLowerCase().equals(id.toLowerCase())){
                        find = true;
                        res = arrayObj.getJSONObject(i);
                        res.put(JSONTOKEN_TYPE_OBJECT,JSONTOKEN_FILMS);
                    }
                }

                //people
                if(find != true){
                    arrayObj = db.getJSONArray(JSONTOKEN_PEOPLE);

                    for (int i = 0; i < arrayObj.length(); i++) {
                        if(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID).toLowerCase().equals(id.toLowerCase())){
                            find = true;
                            res = arrayObj.getJSONObject(i);
                            res.put(JSONTOKEN_TYPE_OBJECT,JSONTOKEN_PEOPLE);
                        }
                    }
                }


                //locations
                if(find != true){
                    arrayObj = db.getJSONArray(JSONTOKEN_LOCATIONS);

                    for (int i = 0; i < arrayObj.length(); i++) {
                        if(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID).toLowerCase().equals(id.toLowerCase())){
                            find = true;
                            res = arrayObj.getJSONObject(i);
                            res.put(JSONTOKEN_TYPE_OBJECT,JSONTOKEN_LOCATIONS);
                        }
                    }
                }


                //species
                if(find != true){
                    arrayObj = db.getJSONArray(JSONTOKEN_SPECIES);

                    for (int i = 0; i < arrayObj.length(); i++) {
                        if(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID).toLowerCase().equals(id.toLowerCase())){
                            find = true;
                            res = arrayObj.getJSONObject(i);
                            res.put(JSONTOKEN_TYPE_OBJECT,JSONTOKEN_SPECIES);
                        }
                    }
                }


                //vehicles
                if(find != true){
                    arrayObj = db.getJSONArray(JSONTOKEN_VEHICLES);


                    for (int i = 0; i < arrayObj.length(); i++) {
                        if(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID).toLowerCase().equals(id.toLowerCase())){
                            find = true;
                            res = arrayObj.getJSONObject(i);
                            res.put(JSONTOKEN_TYPE_OBJECT,JSONTOKEN_VEHICLES);
                        }
                    }
                }


            } catch (JSONException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_DATABASE,TAG_METFUNC_GETJSONOBJECTFROMID), e.getMessage(), e);
            }

            return res;
        }

    /**
     * This function get the Item that have the id put in parameter
     *
     * @param id The id of the item we want to get the from our item list
     * @return the Item with the id put in parameter
     */
    public static Item getItemFromId(String id){
            Item res = null;

            int cpt =0;
            boolean find = false;

            while(cpt< items.size() && find != true){
                if (items.get(cpt).getId().toLowerCase().equals(id.toLowerCase())){
                    find = true;
                    res = items.get(cpt);
                }
                cpt++;
            }

            return res;
        }

    /**
     * This method displays how much data we have for each types it is used for debugging purpose
     */
    //DISPLAY
        public static void dispStats(){
        try {
            JSONArray film = db.getJSONArray(JSONTOKEN_FILMS);
            JSONArray people = db.getJSONArray(JSONTOKEN_PEOPLE);
            JSONArray  locations = db.getJSONArray(JSONTOKEN_LOCATIONS);
            JSONArray species = db.getJSONArray(JSONTOKEN_SPECIES);
            JSONArray vehicles = db.getJSONArray(JSONTOKEN_VEHICLES);

            System.out.println(String.format("%s = %s,%s = %s,%s = %s,%s = %s,%s = %s",JSONTOKEN_FILMS,film.length(),JSONTOKEN_PEOPLE,people.length(),JSONTOKEN_LOCATIONS,locations.length(),JSONTOKEN_SPECIES,species.length(),JSONTOKEN_VEHICLES,vehicles.length()));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    /**
     * This method loads the Items of the Studio Ghibli's API into our own Item list
     *
     * @param jsonObject represent the date fetched from the Ghibli API
     */
        //OTHER
        public static void loadItems(JSONObject jsonObject){
            try {
                JSONArray arrayObj;

                //Film
                arrayObj = jsonObject.getJSONArray(JSONTOKEN_FILMS);
                for(int i = 0; i< arrayObj.length();i++){
                    Item tempItemp = new Item(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID),arrayObj.getJSONObject(i).getString(JSONTOKEN_ORIGINAL_TITLE_ROMAN));
                    items.add(tempItemp);
                }

                //People
                arrayObj = jsonObject.getJSONArray(JSONTOKEN_PEOPLE);
                for(int i = 0; i< arrayObj.length();i++){
                    Item tempItemp = new Item(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID),arrayObj.getJSONObject(i).getString(JSONTOKEN_NAME));
                    items.add(tempItemp);
                }

                //Locations
                arrayObj = jsonObject.getJSONArray(JSONTOKEN_LOCATIONS);
                for(int i = 0; i< arrayObj.length();i++){
                    Item tempItemp = new Item(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID),arrayObj.getJSONObject(i).getString(JSONTOKEN_NAME));
                    items.add(tempItemp);
                }

                //Vehicles
                arrayObj = jsonObject.getJSONArray(JSONTOKEN_VEHICLES);
                for(int i = 0; i< arrayObj.length();i++){
                    Item tempItemp = new Item(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID),arrayObj.getJSONObject(i).getString(JSONTOKEN_NAME));
                    items.add(tempItemp);
                }

                //Species
                arrayObj = jsonObject.getJSONArray(JSONTOKEN_SPECIES);
                for(int i = 0; i< arrayObj.length();i++){
                    Item tempItemp = new Item(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID),arrayObj.getJSONObject(i).getString(JSONTOKEN_NAME));
                    items.add(tempItemp);
                }

            } catch (JSONException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_DATABASE,TAG_METFUNC_LOADITEMS), e.getMessage(), e);
            }

        }

    /**
     * This function get the movies
     *
     * @return a list of movies
     */
    public static List<Item> getMovies(){
            List<Item> res = new ArrayList<>();
            JSONArray arrayObj;

            //Film
            try {
                arrayObj = db.getJSONArray(JSONTOKEN_FILMS);

                for(int i = 0; i< arrayObj.length();i++){
                    Item tempItemp = new Item(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID),arrayObj.getJSONObject(i).getString(JSONTOKEN_ORIGINAL_TITLE_ROMAN));
                    res.add(tempItemp);
                }

            } catch (JSONException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_DATABASE,TAG_METFUNC_GETMOVIES), e.getMessage(), e);
            }
            return res;
        }

    /**
     * This function get the people
     *
     * @return a list of people
     */
    public static List<Item> getPeople(){
        List<Item> res = new ArrayList<>();
        JSONArray arrayObj;

        //Film
        try {
            arrayObj = db.getJSONArray(JSONTOKEN_PEOPLE);

            for(int i = 0; i< arrayObj.length();i++){
                Item tempItemp = new Item(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID),arrayObj.getJSONObject(i).getString(JSONTOKEN_NAME));
                res.add(tempItemp);
            }

        } catch (JSONException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_DATABASE,TAG_METFUNC_GETPEOPLE), e.getMessage(), e);
        }
        return res;
    }

    /**
     * This function get the species
     *
     * @return a list of species
     */
    public static List<Item> getSpecies(){
        List<Item> res = new ArrayList<>();
        JSONArray arrayObj;

        //Film
        try {
            arrayObj = db.getJSONArray(JSONTOKEN_SPECIES);

            for(int i = 0; i< arrayObj.length();i++){
                Item tempItemp = new Item(arrayObj.getJSONObject(i).getString(JSONTOKEN_ID),arrayObj.getJSONObject(i).getString(JSONTOKEN_NAME));
                res.add(tempItemp);
            }

        } catch (JSONException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_DATABASE,TAG_METFUNC_GETSPECIES), e.getMessage(), e);
            e.printStackTrace();
        }
        return res;
    }
}
