package com.example.projetand.asynctask;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.example.projetand.models.Item;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.projetand.utils.Constants.*;


/**
 * This class performs an AsynTask that downloads an image given its URL
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class AsyncBitmapDownloader extends AsyncTask<String,Void, Bitmap> {

    private Item item;
    private ImageView img;

    public AsyncBitmapDownloader(Item item,ImageView img) {
        this.item = item;
        this.img = img;
    }

    /**
     * This function performs the HTTP connection and creates the Bitmap object with the link given
     * @param strings represent the url of the image to download
     */
    @Override
    protected Bitmap doInBackground(String... strings) {
        Bitmap res = null;
        URL url = null;
        try {
            url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                res = BitmapFactory.decodeStream(in);
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCBITMAPDOWNLOADER,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        } catch (IOException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCBITMAPDOWNLOADER,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        }
        return res;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * This method is called once the 'DoInBackground' method is done.
     * It set the bitmap to the ImageView
     *
     * @param bitmap downloaded by the doinbackground function
     */
    @Override
    protected void onPostExecute(Bitmap bitmap) {
        item.setImg(bitmap);
        img.setImageBitmap(bitmap);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(Bitmap bitmap) {
        super.onCancelled(bitmap);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
