package com.example.projetand.asynctask;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.example.projetand.models.Item;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.projetand.helpers.Helpers.*;
import static com.example.projetand.utils.Constants.*;

/**
 * This class performs an AsynTask that get us the link of images needed with the Wallhaven API
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class AsyncGhibliJSONUrl extends AsyncTask<String, Void, JSONObject>  {

    private Item item;
    private ImageView img;

    public AsyncGhibliJSONUrl(Item item,ImageView img) {
        this.item = item;
        this.img = img;
    }

    public AsyncGhibliJSONUrl() {
        this.item = null;
        this.img = null;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * This function performs the HTTP connection and request the JSON data in Wallhaven's API
     * @param strings
     * @return
     */
    @Override
    protected JSONObject doInBackground(String... strings) {

        JSONObject objJ = null;
        URL url = null;
        try {
            url = new URL(strings[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String s = ReadStream(in);
                objJ = new JSONObject(s);
            } catch (JSONException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCGHIBLIJSONURL,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
            } finally {
                urlConnection.getErrorStream();
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCGHIBLIJSONURL,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        } catch(FileNotFoundException e){
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCGHIBLIJSONURL,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        } catch (IOException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCGHIBLIJSONURL,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
        }
        return objJ;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * This method is called once the 'DoInBackground' method is done.
     * We get the image link  from the Wallhaven API and then create an object of
     * type AsyncBitmapDownloader that will download and show the image
     *
     * @param jsonObject represent the answer with all the data given by the API such as the link of the images or the numbers of results
     */
    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        try {

            JSONArray data = jsonObject.getJSONArray(JSONTOKEN_DATA);

            if(data.length()<=0){
                new AsyncBitmapDownloader(item,img).execute(PROPERTIES_DEFAULT_IMG_URL);

            }else{
                String url = data.getJSONObject(0).getString(JSONTOKEN_PATH);
                new AsyncBitmapDownloader(item,img).execute(url);
            }

        } catch (JSONException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCGHIBLIJSONURL,TAG_METFUNC_ONPOSTEXECUTE), e.getMessage(), e);
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(JSONObject s) {
        super.onCancelled(s);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
