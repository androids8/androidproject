package com.example.projetand.asynctask;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.projetand.models.Database;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.projetand.utils.Constants.*;
import static com.example.projetand.helpers.Helpers.*;

/**
 * This class performs an AsynTask that populate the JSON  object in our Database class by getting connecting to the Studio
 * Ghibli's API. It also shows the user a progressBar that tells him how far the advancement of the data loading is.
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class AsyncGhibliJSON extends AsyncTask<String,Integer, JSONObject> {

    private ProgressBar progressBar;
    private TextView textViewResult;
    private Button btn_continue;

    public AsyncGhibliJSON(ProgressBar progressBar, TextView textViewResult,Button btn_continue) {
        super();
        this.progressBar = progressBar;
        this.textViewResult = textViewResult;
        this.btn_continue = btn_continue;
    }
    /**
     * This function performs the HTTP connection and request the JSON data in Studio Ghibli's API
     * @param strings represent all the url of the different endpoints of the studio Ghibli API
     */
    @Override
    protected JSONObject doInBackground(String... strings) {
        /*Definition JSONObject*/
        JSONObject root = new JSONObject();

        for(String string : strings) {
            try {
                URL url = new URL(string);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String s = ReadStream(in);
                root.put(getIdFromUrl(string),new JSONArray(s));

                publishProgress(1);

                urlConnection.disconnect();

            } catch (JSONException | MalformedURLException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCGHIBLIJSON,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
            } catch (IOException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_ASYNCGHIBLIJSON,TAG_METFUNC_DOINBACKGROUND), e.getMessage(), e);
            }
        }

        return root;
    }

    /**
     * This method shows the user that the app is downloading the data before performing the task to inform the user.
     */
    @Override
    protected void onPreExecute() {
        textViewResult.setText("Downloading");
        super.onPreExecute();
    }

    /**
     * This method is called once the 'DoInBackground' method is done.
     * It populates the JSON Object in our Database and it also creating our items list.
     *
     * @param jsonObject fetched from the Ghibli API
     */
    @Override
    protected void onPostExecute(JSONObject jsonObject) {

        Database.setDb(jsonObject);
        textViewResult.setText("Downloaded");
        textViewResult.setText("Converting");
        Database.loadItems(jsonObject);
        textViewResult.setText("Converted");
        textViewResult.setText("Finish");
        btn_continue.setVisibility(View.VISIBLE);
        super.onPostExecute(jsonObject);
    }

    /**
     * This method shows the progress of the update by updating the state of the progress bar
     * @param values value to increment the progress of the bar
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        progressBar.setProgress(progressBar.getProgress()+values[0],true);
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(JSONObject jsonObject) {
        super.onCancelled(jsonObject);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }


}
