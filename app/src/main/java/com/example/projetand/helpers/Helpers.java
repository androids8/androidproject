package com.example.projetand.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

import static com.example.projetand.utils.Constants.*;

/**
 * This class is used to avoid the repetition of some functions or methods in the program.
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class Helpers {

    /**
     * This function is used to read the input and to translate the input into a String to be processed.
     *
     * @param is it is the input that we want to read and to convert to a String
     * @return String return a string representation of the input given
     */
    public static String ReadStream(InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_HELPER,TAG_METFUNC_READSTREAM), e.getMessage(), e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e(String.format("[%s]-[%s] : ", TAG_CLASSNAME_HELPER,TAG_METFUNC_READSTREAM), e.getMessage(), e);
            }
        }
        return sb.toString();
    }

    /**
     * This function is used to format the url by using the name of the item
     *
     * @param name it is the name of an item
     * @return String that represent the formated URL for the Ghibli API
     */
    public static String formatUrlFromName(String name){
        return String.format("%s%s%s&%s%s",PROPERTIES_URL_API_SEARCH,name.toLowerCase().replaceAll(" ","%20").replaceAll("'",""),"&sorting=relevance","apikey=",PROPERTIES_API_KEY);
    }

    /**
     * This function is used to check if the endpoint is in the list or not
     *
     * @param url it is the name the endpoint it can be an id
     * @return boolean
     */
    public static boolean isUrlValid(String url){
        return !Arrays.asList(PROPERTIES_DEFAULTS_ENDPOINTS).contains(getIdFromUrl(url).toLowerCase());
    }

    /**
     * This function is used to get the id from the url
     *
     * @param url it is the url of an object
     * @return String that represent the id
     */
    public static String getIdFromUrl(String url){
        String[] parts = url.split("/");
        String endpoint = parts[parts.length-1];

        return endpoint;
    }

    /**
     * This function is used to check if the permissions that we need to ask for the use are granted or not.
     *
     * @param context it is the context
     * @return boolean return true if the permission are not yet accepted by the user
     */
    public static boolean checkPermissionGranted(Context context){
        boolean fine_location = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
        boolean coarse_location =  ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;

        return (fine_location && coarse_location);
    }

    /**
     * This method is used to ask for the permissions to use some permissions.
     *
     * @param activity it is the activity
     */
    public static void askPermissions(Activity activity){
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE};
        ActivityCompat.requestPermissions(activity, permissions, 0);
    }

    /**
     * This method is used to check if the user choose light mode or dark mode
     *
     * @param activity is the activity
     * @param switchCompat is the switch used to check if the user want or not the dark mode
     */
    public static void changeModeOfApplication(Activity activity, SwitchCompat switchCompat){
        SharedPreferences sharedPref = activity.getSharedPreferences(SHAREDPREFTOKEN_SHAREDPREF, activity.MODE_PRIVATE);

        if(sharedPref.getBoolean(SHAREDPREFTOKEN_DARKMODE, true)){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            if(switchCompat != null){
                switchCompat.setChecked(true);
            }
        }
        else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            if(switchCompat != null){
                switchCompat.setChecked(false);
            }
        }
    }

}
