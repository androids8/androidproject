package com.example.projetand.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projetand.R;
import com.example.projetand.models.Database;
import com.example.projetand.models.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is extending the BaseAdapter abstract class that let us customize the GridView
 * Used for the HomeFragment
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class CustomAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private List<Item> itemsDefault = new ArrayList<Item>();
    private List<Item> itemsFiltered = new ArrayList<Item>();

    public CustomAdapter(Context c,List<Item> itemsDefault) {
        this.itemsDefault = itemsDefault;
        mContext = c;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return itemsDefault.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return itemsDefault.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * Get a View that displays the data at the specified position in the data set.
     * @param position
     * @param convertView
     * @param parent
     * @return the View inflated and customized to have a TextView and a ImageView in each grid_view
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View vw_inflated = convertView;

        Item tempItem = itemsDefault.get(position);

        if(vw_inflated == null){
            vw_inflated = LayoutInflater.from(mContext).inflate(R.layout.grid_view_sample,parent,false);
        }

        TextView lbl_name = (TextView) vw_inflated.findViewById(R.id.sample_text);
        ImageView img_movie = (ImageView)vw_inflated.findViewById(R.id.sample_image);

        lbl_name.setText(tempItem.getName());
        img_movie.setImageResource(R.drawable.totoro);

        return vw_inflated;
    }

    /**
     * This method reset the state of the items lists
     */
    public void refresh(){
        itemsDefault = Database.getItems();
        itemsFiltered =  Database.getItems();
    }

    /**
     * This function get the keywords put by the user in the search bar and filter the items depending of the user's keyword
     * @return a filter
     */
    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();

                if(constraint == null || constraint.length() <= 0){
                    filterResults.count = itemsFiltered.size();
                    filterResults.values = itemsFiltered;
                }else{
                    String keyword = constraint.toString().toLowerCase();
                    List<Item> tempResult = new ArrayList<Item>();

                    for (Item item: itemsDefault){
                        if(item.getName().toLowerCase().contains(keyword)){tempResult.add(item);}
                    }

                    filterResults.count = tempResult.size();
                    filterResults.values = tempResult;
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itemsDefault = (List<Item>) results.values;
                notifyDataSetChanged();
            }
        };

        return filter;
    }
}
