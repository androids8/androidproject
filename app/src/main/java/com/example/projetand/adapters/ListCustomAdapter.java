package com.example.projetand.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projetand.R;
import com.example.projetand.models.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is extending the BaseAdapter abstract class that let us customize the ListView
 * Used for the MovieFragment, PeopleFragment and SpeciesFragment
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */

public class ListCustomAdapter extends BaseAdapter {

    private Context mContext;
    private List<Item> itemsDefault = new ArrayList<Item>();

    public ListCustomAdapter(Context mContext, List<Item> itemsDefault) {
        this.mContext = mContext;
        this.itemsDefault = itemsDefault;
    }

    @Override
    public int getCount() {
        return itemsDefault.size();
    }

    @Override
    public Object getItem(int position) {
        return itemsDefault.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Get a View that displays the data at the specified position in the data set.
     * @param position
     * @param convertView
     * @param parent
     * @return the View inflated and customized to have a TextView and a ImageView in each grid_view
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vw_inflated = convertView;

        Item tempItem = itemsDefault.get(position);

        if(vw_inflated == null){
            vw_inflated = LayoutInflater.from(mContext).inflate(R.layout.list_view_sample,parent,false);
        }

        TextView lbl_name = (TextView) vw_inflated.findViewById(R.id.sample_listview_text);
        ImageView img_movie = (ImageView)vw_inflated.findViewById(R.id.sample_listview_image);

        lbl_name.setText(tempItem.getName());
        img_movie.setImageResource(R.drawable.totoro);

        return vw_inflated;
    }
}
