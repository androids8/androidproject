package com.example.projetand.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.projetand.R;
import com.example.projetand.activities.DetailsActivity;
import com.example.projetand.adapters.ListCustomAdapter;
import com.example.projetand.models.Database;
import com.example.projetand.models.Item;
import com.example.projetand.services.CustomService;

import static com.example.projetand.utils.Constants.*;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 * @author  Tristan CLEMENCEAU Alex GOMEZ
 * @since   2021-03-17
 */
public class MovieFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ListView listv;
    private ListCustomAdapter adapter;

    public MovieFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MovieFragment.
     */
    public static MovieFragment newInstance(String param1, String param2) {
        MovieFragment fragment = new MovieFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    /**
     * This function is called to have the fragment instantiate the view.
     * Here we will display only the movies
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_movie, container, false);

        adapter = new ListCustomAdapter(v.getContext(), Database.getMovies());
        listv= (ListView) v.findViewById(R.id.list_view_movie);

        listv.setAdapter(adapter);

        listv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Item item = (Item) adapter.getItem(position);

                final Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra(EXTRATOKEN_ID_ITEM,item.getId());

                Intent service = new Intent(getActivity(), CustomService.class);
                service.putExtra(EXTRATOKEN_SERVICE_VALUE,item.getId());
                service.putExtra(EXTRATOKEN_SERVICE_JSON,PROPERTIES_DEFAULT_TRUE);

                getActivity().startService(service);
                startActivity(intent);
                getActivity().finish();
            }
        });

        return v;
    }
}