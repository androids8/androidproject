package com.example.projetand.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SearchView;

import com.example.projetand.R;
import com.example.projetand.activities.DetailsActivity;
import com.example.projetand.adapters.CustomAdapter;
import com.example.projetand.models.Database;
import com.example.projetand.models.Item;
import com.example.projetand.services.CustomService;

import static com.example.projetand.utils.Constants.*;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private GridView grid;
    private SearchView srv;
    private CustomAdapter adapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    /**
     * This function is called to have the fragment instantiate the view.
     * Here we will display everything : movies, peoples, locations, species, vehicles
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_home, container, false);

        adapter = new CustomAdapter(v.getContext(),Database.getItems());
        grid=(GridView) v.findViewById(R.id.grid_view);
        srv =(SearchView) v.findViewById(R.id.search_view);

        grid.setAdapter(adapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Item item = (Item) adapter.getItem(position);

                final Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra(EXTRATOKEN_ID_ITEM,item.getId());

                Intent service = new Intent(getActivity(), CustomService.class);
                service.putExtra(EXTRATOKEN_SERVICE_VALUE,item.getId());
                service.putExtra(EXTRATOKEN_SERVICE_JSON,PROPERTIES_DEFAULT_TRUE);

                getActivity().startService(service);
                startActivity(intent);
                getActivity().finish();
            }
        });

        srv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            /**
             * This function check if the user submitted the query sent in the search bar
             * If he submit data, it will start a Service that will keep the history of his search bar
             *
             * @param query Keyword given
             */
            @Override
            public boolean onQueryTextSubmit(String query) {

                /*SERVICE*/
                Intent service = new Intent(getActivity(),CustomService.class);
                service.putExtra(EXTRATOKEN_SERVICE_VALUE,query);
                service.putExtra(EXTRATOKEN_SERVICE_JSON,PROPERTIES_DEFAULT_FALSE);

                getActivity().startService(service);
                return false;
            }

            /**
             * This function checks if the used modified or is writing in the searchbar
             * If he did, it will refresh the view with the filter he wrote
             *
             * @param newText keyword given
             */
            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.refresh();
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        return v;
    }
}